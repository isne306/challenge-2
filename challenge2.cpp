//CHALLENGE #2

//Connect 4!

//Connect 4 is a game played on a grid with 7 columns and 6 rows. 
//Players take it in turns to drop a symbol into a column – i.e. players choose a column between 1-7. 
//The symbol will drop to the lowest empty row.
// The game is won if a player connects 4 of their symbols in a row, either horizontally, vertically or diagonally.

//Your program should allow 2 players to take alternate turns to drop their symbols.
// After each turn the state of the board should be displayed so the next player can choose their column.
//Your program should announce the winner if a player manages to get 4 in a row!

#include <iostream>
using namespace std;

void PrintBoard (char board[6][7])
{
	for (int i=0;i<6;i++)
	{
		cout << "|";
		for (int j=0;j<7;j++)
		{
			cout << " "<< board[i][j] << " |" ;
		}
		cout << endl;
		cout << "-----------------------------";
		cout << endl;
	}
}

void BoardInit (char board[6][7])
{
	for (int i=0;i<6;i++)
	{
		for (int j=0;j<7;j++)
		{
			board[i][j]=' ';
		}
	}
}

void Input(int player,int &num,char board[6][7])
{
	do
	{
		cout << "Player " << player << " Input number (1-7): ";
		cin >> num;
		if (num < 1 || num > 7)
		{
			cout << "Error !!! Plase Try agin (1-7)" << endl;
		}			
		if (board[0][num-1] == 'X' || board[0][num-1] == 'O')
		{
			cout << "This column is FULL! Choose another column " << endl;				
		}
		
	} while ((num < 1 || num > 7 ) || board[0][num-1] != ' ' );

}

void UpdateBoard(char board[6][7],int &num,int player)
{
	for ( int i=0;i<6;i++)
	{			
		if (board[5-i][num-1] == ' ')
		{
			if(player==1)
			{
				board[5-i][num-1] = 'X';					
			}
			else 
			{
				board[5-i][num-1] = 'O';					
			}
			break;
		}
		
	}
}

void CheckWin(char board[6][7],int player,int &win)
{
	for (int j=6;j>=0;j--)
	{
		for (int i=5;i>=3;i--)
		{
			if (board[i][j]==board[i-1][j] && board[i-1][j]==board[i-2][j] && board[i-2][j]==board[i-3][j] && board[i][j]!=' ')
			{

				cout << "Player " << player << " WIN" << endl;
				win = 1;
				break;
			}
		}
	}
	for (int i=5;i>=0;i--)
	{
		for (int j=6;j>=3;j--)
		{
			if (board[i][j]==board[i][j-1] && board[i][j-1]==board[i][j-2] && board[i][j-2]==board[i][j-3] && board[i][j]!=' ')
			{
				cout << "Player " << player << " WIN" << endl;
				win = 1;
				break;
			}
		}
	}
	
	for (int k=0;k<3;k++)
	{
		if (board[5-k][k]==board[5-k-1][k+1] && board[5-k-1][k+1]==board[5-k-2][k+2] && board[5-k-2][k+2]==board[5-k-3][k+3] && board[5-k-3][k+3]!=' ')
		{
			cout << "Player " << player << " WIN" << endl;
			win = 1;
			break;
		}
	}
	for (int k=0;k<2;k++)
	{
		if (board[4-k][k]==board[4-k-1][k+1] && board[4-k-1][k+1]==board[4-k-2][k+2] && board[4-k-2][k+2]==board[4-k-3][k+3] && board[4-k-3][k+3]!=' ')
		{
			cout << "Player " << player << " WIN" << endl;
			win = 1;
			break;
		}
	}
	if (board[3][0]==board[2][1] && board[2][1]==board[1][2] && board[1][2]==board[0][3] && board[0][3]!=' ')
	{
		cout << "Player " << player << " WIN" << endl;
		win = 1;		
	}
	for (int k=0;k<3;k++)
	{
		if(board[5-k][k+1]==board[5-k-1][k+2] && board[5-k-1][k+2]==board[5-k-2][k+3] && board[5-k-2][k+3]==board[5-k-3][k+4] && board[5-k-3][k+4]!=' ')
		{
			cout << "Player " << player << " WIN" << endl;
			win = 1;
			break;
		}
	}
	for(int k=0;k<2;k++)
	{
		if(board[5-k][k+2]==board[5-k-1][k+3] && board[5-k-1][k+3]==board[5-k-2][k+4] && board[5-k-2][k+4]==board[5-k-3][k+5] && board[5-k-3][k+5]!=' ')
		{
			cout << "Player " << player << " WIN" << endl;
			win = 1;
			break;
		}
	}
	if (board[5][3]==board[4][4] && board[4][4]==board[3][5] && board[3][5]==board[2][6] && board[2][6]!=' ')
	{
		cout << "Player " << player << " WIN" << endl;
		win = 1;
	}
	if (board[2][0]==board[3][1] && board[3][1]==board[4][2] && board[4][2]==board[5][3] && board[2][0]!=' ')
	{
		cout << "Player " << player << " WIN" << endl;
		win = 1;
	}
	if (board[0][3]==board[1][4] && board[1][4]==board[2][5] && board[2][5]==board[3][6] && board[3][6]!=' ')
	{
		cout << "Player " << player << " WIN" << endl;
		win = 1;
	}
	for (int k=0;k<2;k++)
	{
		if (board[k+1][k]==board[k+2][k+1] && board[k+2][k+1]==board[k+3][k+2] && board[k+3][k+2]==board[k+4][k+3] && board[k+4][k+3]!=' ')
		{
			cout << "Player " << player << " WIN" << endl;
			win = 1;
			break;
		}
	}
	for (int k=0;k<2;k++)
	{
		if (board[k][k+2]==board[k+1][k+3] && board[k+1][k+3]==board[k+2][k+4] && board[k+2][k+4]==board[k+3][k+5] && board[k+3][k+5]!=' ')
		{
			cout << "Player " << player << " WIN" << endl;
			win = 1;
			break;
		}
	}
	for (int k=0;k<3;k++)
	{
		if(board[k][k]==board[k+1][k+1] && board[k+1][k+1]==board[k+2][k+2] && board[k+2][k+2]==board[k+3][k+3] && board[k+3][k+3]!=' ')
		{
			cout << "Player " << player << " WIN" << endl;
			win = 1;
			break;
		}
	}
	for (int k=0;k<3;k++)
	{
		if(board[k][k+1]==board[k+1][k+2] && board[k+1][k+2]==board[k+2][k+3] && board[k+2][k+3]==board[k+3][k+4] && board[k+3][k+4]!=' ')
		{
			cout << "Player " << player << " WIN" << endl;
			win = 1;
			break;
		}
	}
}

void SwapPlayer(int &player)
{
	if (player == 1)
	{
		player = 2;
	}
	else
	{
		player = 1;
	}
}

int main()
{
	// variables
	char board[6][7]={};
	int num=0;
	int player=1;
	int win=0;

	// board init
	BoardInit(board);
	
	// print board
	PrintBoard(board);

	for (int i=0;i<42;i++)
	{

		// Input (1-7)
		Input(player,num,board);

		// update board
		UpdateBoard(board,num,player);

		// print updated board
		PrintBoard(board);

		// check win
		CheckWin(board,player,win);

		if (win == 1 )
		{
			break;
		}

		//swap player
		SwapPlayer(player);
	}

	if (win == 0)
	{
		cout << " This game is DRAW";
	}
	

	return 0;
}	